﻿using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseMenu : MonoBehaviour
{
    private CursorLockMode previousCursorLoackState;
    private bool previousCursorVisibility;
    private float previousTimeScale;

    private void OnEnable()
    {
        previousCursorLoackState = Cursor.lockState;
        previousCursorVisibility = Cursor.visible;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        previousTimeScale = Time.timeScale;
        Time.timeScale = 0;
    }

    private void OnDisable()
    {
        Cursor.lockState = previousCursorLoackState;
        Cursor.visible = previousCursorVisibility;

        Time.timeScale = previousTimeScale;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Start") || Input.GetButtonDown("Cancel"))
            gameObject.SetActive(false);
    }

    public void RestartLevelClick()
    {
        transform.root.GetComponent<Level>()?.RestartLevel();
    }

    public void MainMenuClick()
    {
        SceneManager.LoadScene("Welcome");
    }

    public void QuitClick()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
}
