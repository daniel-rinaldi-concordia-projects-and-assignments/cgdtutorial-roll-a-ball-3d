﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public GameObject player;
    [Tooltip("Restarts the level if the player falls beneath this height.")]
    public float levelRestartHeight = -10;

    public Text txtScore;

    public PauseMenu pauseMenu;

    private int score;
    public int Score 
    {
        get => score;
        set
        {
            score = value;
            txtScore.text = score.ToString();
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Start"))
            pauseMenu.gameObject.SetActive(true);

        if (player.transform.position.y < levelRestartHeight)
            RestartLevel();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
